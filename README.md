# PersonDBGenerator

Genera una base de datos en SQLite con registros de personas ficticias utilizando la API de https://randomuser.me/.

La estructura de la misma se puede encontrar en el archivo de este repositorio ubicado en "Model/Person.cs".

![DatabaseGenerator](https://user-images.githubusercontent.com/93444165/139864069-5b5b71e8-f123-41af-8fe9-df93c0bd0e3d.gif)

- .NET 5.0
- Aplicación de consola
- C#
- Entity Framework 6
- SQLite
